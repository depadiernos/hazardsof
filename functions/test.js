/**
 * Take the body from a deploy succeeded webhook and start tests using
 * GitLab CI Pipelines
 */

exports.handler = async function(event, context) {

	// Log the current date and time
	console.log(`Handling today`);
    
	const pretend = (arg) => console.log(request)
	// Shared done callback
	const done = () => { return { statusCode: 200, body: 'Done' } }

	// Make the URL to the page
	const body = JSON.parse(event.body)
	const buildUrl = `https://${body.id}--${body.name}.netlify.com`
	console.log('Build URL', buildUrl)
	// Build the request
	const request = {
		method: 'post',
		baseURL: 'https://gitlab.com/api/v4',
		url: `/projects/${process.env.GITLAB_CI_PROJECT_ID}/trigger/pipeline`,
		params: {

			// Gitlab CI auth variables
			token: process.env.GITLAB_CI_TOKEN,
			ref: process.env.GITLAB_CI_REF,
			'variables[BUILD_URL]': buildUrl,
			'variables[COMMIT_REF]': body.commit_ref,
			'variables[COMMIT_TITLE]': body.title,
		}
	}

	// Trigger build
	console.log('Triggering');
	await pretend(request);

	// Report done
	return done();
};